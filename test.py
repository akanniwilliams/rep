#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Dump all replication events from a remote mysql server

from pymysqlreplication import BinLogStreamReader
MYSQL_SETTINGS = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "passwd": "root"
}

def main():
    stream = BinLogStreamReader(connection_settings=MYSQL_SETTINGS,
                                server_id=100,
                                blocking=True)

    for binlogevent in stream:
        binlogevent.dump()



    stream.close()

if __name__ == "__main__":

    main()
